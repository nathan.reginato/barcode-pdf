package main

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/png"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code39" // This is the same barcode generation algorithm used by the monolith
	"github.com/signintech/gopdf"
)

const (
	PersonID      = "12345"
	BarcodeHeight = 400
	BarcodeWidth  = 200
)

var (
	PageSize = gopdf.PageSizeLetter
)

func main() {
	// Create the barcode
	barcodeCheckSum, err := code39.Encode(PersonID, false, false)
	if err != nil {
		fmt.Printf("failed encoding PersonID: %s", err)
	}
	scaledBarcode, err := barcode.Scale(barcodeCheckSum, BarcodeHeight, BarcodeWidth)
	if err != nil {
		fmt.Printf("failed to scale barcode: %s", err)
	}

	// Convert barcode image to greyscalse model. The barcode library outputs
	// a 16-bit depth image, but the PDF library only supports 8-bit depth:w
	greyScaleImage := &Converted{scaledBarcode, color.GrayModel}

	// Encode PNG into buffer
	imageBuffer := new(bytes.Buffer)
	err = png.Encode(imageBuffer, greyScaleImage)
	if err != nil {
		fmt.Printf("failed to encode barcode to PNG: %s", err)
	}

	// Creat new PDF and configure
	pdf := gopdf.GoPdf{}
	pdf.Start(gopdf.Config{PageSize: *PageSize})
	pdf.AddPage()

	pdf.AddTTFFont("daysone", "example-font.ttf")
	pdf.SetFont("daysone", "", 20)

	// Import existing PDF -- this can also be a stream instead of a file
	tpl1 := pdf.ImportPage("pdf-test.pdf", 1, "/MediaBox")
	pdf.UseImportedTemplate(tpl1, 0, 0, PageSize.W, PageSize.H)

	// Rotate to prepare for barcode
	pdf.Rotate(90.0, 0, 0)

	const barcodeOffsetY float64 = 50
	const barcodeOffsetX float64 = 30

	// Use image in PDF
	ih, err := gopdf.ImageHolderByReader(imageBuffer)
	if err != nil {
		fmt.Printf("failed to import image: %s", err)
	}
	err = pdf.ImageByHolder(ih, -PageSize.H+barcodeOffsetY, PageSize.W-barcodeOffsetX, nil)
	if err != nil {
		fmt.Printf("failed to add image to PDF: %s", err)
	}

	// Write PDF
	pdf.WritePdf("output.pdf")

}

type Converted struct {
	Img image.Image
	Mod color.Model
}

func (c *Converted) ColorModel() color.Model {
	return c.Mod
}

func (c *Converted) Bounds() image.Rectangle {
	return c.Img.Bounds()
}

func (c *Converted) At(x, y int) color.Color {
	return c.Mod.Convert(c.Img.At(x, y))
}
