module gitlab.com/nathan.reginato/barcode

go 1.15

require (
	github.com/boombuler/barcode v1.0.1
	github.com/signintech/gopdf v0.9.13
)
